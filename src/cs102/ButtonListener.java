package cs102;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ButtonListener implements ActionListener {
    private ArrayList<JRadioButton> radioButtons;

    public ButtonListener(ArrayList<JRadioButton> radioButtons) {
        this.radioButtons = radioButtons;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        String message = "Status:";
        for (JRadioButton radioButton : radioButtons) {
            message += "," + radioButton.isSelected();
        }
        JOptionPane.showMessageDialog(null, message.replaceFirst(",", ""));
    }
}
