package cs102;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Simple Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLayout(new BorderLayout());

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(6, 1));
        frame.add(mainPanel);

        JRadioButton radioButton1 = new JRadioButton("button 1");
        JRadioButton radioButton2 = new JRadioButton("button 2");
        JRadioButton radioButton3 = new JRadioButton("button 3");
        JRadioButton radioButton4 = new JRadioButton("button 4");
        JRadioButton radioButton5 = new JRadioButton("button 5");

        ButtonGroup buttonGroup1 = new ButtonGroup();
        buttonGroup1.add(radioButton1);
        buttonGroup1.add(radioButton2);
        buttonGroup1.add(radioButton3);

        ButtonGroup buttonGroup2 = new ButtonGroup();
        buttonGroup2.add(radioButton4);
        buttonGroup2.add(radioButton5);

        mainPanel.add(radioButton1);
        mainPanel.add(radioButton2);
        mainPanel.add(radioButton3);
        mainPanel.add(radioButton4);
        mainPanel.add(radioButton5);

        JButton button = new JButton("Report");
        mainPanel.add(button);

        button.addActionListener(new ButtonListener(new ArrayList<JRadioButton>(Arrays.asList(radioButton1,
                radioButton2, radioButton3, radioButton4, radioButton5))));

        frame.setVisible(true);
    }
}
